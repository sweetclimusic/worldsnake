﻿using UnityEngine;

namespace SnakePlanet {

    public class PlanetGravity : MonoBehaviour {
        //UNIVERSAL Gravitation Equation
        // F = G * (m1 * m2) / distance2
        [SerializeField]
        private float G = 10f;
        [SerializeField]
        private float gravityForceFactor = 1000f;
        public float rotationSpeed;

        public float Gravity {
            get {
                return G;
            }

            set {
                G = value;
            }
        }

        public float GravityForceFactor {
            get {
                return gravityForceFactor;
            }

            set {
                gravityForceFactor = value;
            }
        }

        public void Attract(Transform rigidBodyTransform) {
            var gravityUpDirection = (rigidBodyTransform.position - transform.position).normalized;
            var rigidBodyUp = rigidBodyTransform.up;
            var rigidBody = rigidBodyTransform.GetComponent<Rigidbody>();

            rigidBody.AddForce(gravityUpDirection * G);
            // reorientate rigibody
            rigidBody.rotation = Quaternion.FromToRotation(rigidBodyUp, gravityUpDirection) * rigidBody.rotation;

        }
        
    }
}