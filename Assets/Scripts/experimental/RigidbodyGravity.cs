﻿using UnityEngine;

namespace SnakePlanet
{
	[RequireComponent(typeof(Rigidbody))]
	public class RigidbodyGravity : MonoBehaviour
	{
		private PlanetGravity planetCore;
		public Rigidbody rigidBody;
		private Transform localTransform;
		
		void Awake()
		{
			if (!planetCore)
			{
				//fine the core if ommitted
				planetCore = GameObject.FindWithTag("Planet").GetComponent<PlanetGravity>();
			}
			//self rigidbody and up awareness
            localTransform = transform;
            rigidBody = GetComponent<Rigidbody>();
            rigidBody.constraints = RigidbodyConstraints.FreezeRotation;
            rigidBody.useGravity = false;
			
		}

		private void FixedUpdate()
		{
			planetCore.Attract(rigidBodyTransform: localTransform);
		}
	}
}