﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SnakePlanet {
    public class DynamicRigidbodyGravity : MonoBehaviour {
        [SerializeField]
        private Transform planetCoreTransform;
        public float minDistanceFromSurface;
        public float maxDistanceFromSurface;
        public Rigidbody rigidBody;

        private Vector3 planetSurface;
        private float planetRadius;
        // Use this for initialization
        void Awake() {
            rigidBody = GetComponent<Rigidbody>();
            rigidBody.useGravity = false;
            if (!planetCoreTransform) {
                    planetCoreTransform = GameObject.FindWithTag("Planet").GetComponent<Transform>();
                    planetRadius = planetCoreTransform.GetComponent<SphereCollider>().radius * planetCoreTransform.localScale.x;
                }
        }

        // Update is called once per frame
        void Update() {
            //curent player position, find the direction
            
            planetSurface = (planetCoreTransform.position - transform.position) * planetRadius;
            Debug.DrawRay(transform.position, planetSurface, Color.black);

        }
        void FixedUpdate() {
            BodyModifier();
        }
        /// <summary>
        /// Modifys the rigidbodies gravitation distance and orentation to the planet
        /// </summary>
        public void BodyModifier() {
            RaycastHit hit;
            var planetCoreOffset = planetCoreTransform.position - transform.position;
            var planetCoreSqrLength = planetCoreOffset.sqrMagnitude;
            var planetGravity = planetCoreTransform.GetComponent<PlanetGravity>();
#if UNITY_EDITOR
            Debug.DrawRay(transform.position, planetCoreOffset, Color.red);
#endif
            //don't panic unless we're no longer touching the planet's surface then begin panic sequence.
            if (Physics.Raycast(transform.position, planetCoreOffset, out hit, planetCoreSqrLength, LayerMask.NameToLayer("Planet"))) {
                transform.up = hit.normal;
                rigidBody.AddForce(-hit.normal * planetGravity.Gravity, ForceMode.VelocityChange);
            }
           
            if (planetCoreSqrLength > maxDistanceFromSurface) {
                rigidBody.AddForce(-hit.normal * planetGravity.GravityForceFactor);
                rigidBody.drag = 0.5f;
            }
            if (planetCoreSqrLength < minDistanceFromSurface) {
                Debug.Log(planetCoreSqrLength);
                rigidBody.AddForce(hit.normal * planetGravity.Gravity);
            }
        }
    }
}
