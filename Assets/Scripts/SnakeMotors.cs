﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakePlanet {
    [RequireComponent(typeof(Rigidbody))]
    public class SnakeMotors : MonoBehaviour {
        public float _propelStrength;
        public float _propelDistance;
        public Transform[] _motors;
        private PlanetGravity _planetCore;
        private SnakeController _snakeController;

        void Awake() {
            if (!_planetCore) {
                //fine the core if ommitted
                _planetCore = GameObject.FindWithTag("Planet").GetComponent<PlanetGravity>();
                _snakeController = GameObject.FindWithTag("Player").GetComponent<SnakeController>();
            }
        }

        // FixedUpdate framerate independent update
        void FixedUpdate() {
            RaycastHit hit;
            var planetTransform = _planetCore.GetComponent<Transform>();
            var motorRigidBody = gameObject.GetComponent<Rigidbody>();
            var snakeTransform = gameObject.GetComponent<Transform>();

            foreach (var motor in _motors) {
                var gravityUpDirection = (motor.up - planetTransform.position).normalized;
                if ( Physics.Raycast( motor.position,-gravityUpDirection,out hit,_propelDistance,LayerMask.NameToLayer("Planet") ) ) {
                    //find how far away we are from the surface of the planet
                    var distancePrecentage = 1 - (hit.distance / _propelDistance);
                    //use force to push off the surface.
                    var downForce = transform.up * _propelStrength * distancePrecentage;
                    downForce *= Time.deltaTime * motorRigidBody.mass;
                    motorRigidBody.AddForceAtPosition(downForce, motor.position);
                    _snakeController.Grounded = true;
                }else {
                    //return to earth
                    // Self levelling - returns the vehicle to horizontal when not grounded
                    if (snakeTransform.position.y > motor.position.y) {
                        motorRigidBody.AddForceAtPosition(motor.up * _planetCore.Gravity, motor.position);
                    }
                    else {
                        motorRigidBody.AddForceAtPosition(motor.transform.up * -_planetCore.Gravity, motor.position);
                    }
                }

            }
        }
    }
}