﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SnakePlanet {
    public class AppleSpawner : MonoBehaviour {
        public MeshFilter planetMeshFilter;
        public ParticleSystem eatParticleFx;
        private List<Vector3> spawnPoints;
        private SnakeController snakeControllerObject;
        private float radius;
        private IEnumerator fxCoroutine;
        private MeshRenderer appleMeshRender;

        void Awake() {
            //fine the core if ommitted
            snakeControllerObject = GameObject.FindWithTag("Player").GetComponent<SnakeController>();
            if (!eatParticleFx) {
                eatParticleFx = GetComponentInChildren<ParticleSystem>();
            }
            
            //fine the surface if ommitted
            planetMeshFilter = GameObject.FindWithTag("Planet").GetComponent<MeshFilter>();
            //for positioning and hidding the apple
            radius = GetComponent<SphereCollider>().radius;
            appleMeshRender = GetComponent<MeshRenderer>();
        }
        // Use this for initialization
        void Start() {
            //keep track of your coroutines for your neighbourhood garbage collector.
            fxCoroutine = Respawn();
            Build();
            Spawn();
        }

        void Build() {
            spawnPoints = new List<Vector3>();
            var allPoints = new List<Vector3>();
            var planetSurfaceMesh = planetMeshFilter.mesh;
            for (var i = 0; i < planetSurfaceMesh.triangles.Length; i += 3) {
                Vector3 p1 = planetSurfaceMesh.vertices[planetSurfaceMesh.triangles[i + 0]];
                Vector3 p2 = planetSurfaceMesh.vertices[planetSurfaceMesh.triangles[i + 1]];
                Vector3 p3 = planetSurfaceMesh.vertices[planetSurfaceMesh.triangles[i + 2]];

                var centroid = (p1 + p2 + p3) / 3;
                allPoints.Add(centroid);
            }
            //limit to only every 1/3 point from all avalible points and randomly choose which step increment.
            var randomStartIncrement = UnityEngine.Random.Range(0, 3);
            for (var i = randomStartIncrement; i < allPoints.Count; i += 3) {
                spawnPoints.Add(allPoints[i]);
            }
            //mhmm maybe you can use the other 2/3 points in the all points to generate something else later.

        }
        void Spawn() {
            appleMeshRender.enabled = true;
            var appleTransform = transform;
            var gravityUpOffset = (appleTransform.position - transform.position);
            var gravityUpDirection = gravityUpOffset.normalized;
            Debug.DrawRay(transform.position, gravityUpOffset, Color.red);
            appleTransform.SetPositionAndRotation(this.getSafeSpawnPoint(),
                Quaternion.FromToRotation(appleTransform.up, gravityUpDirection) * appleTransform.rotation);
            appleTransform.position += new Vector3(0, radius/2, 0);
        }

        Vector3 getSafeSpawnPoint() {
            var randomIndex = UnityEngine.Random.Range(0, spawnPoints.Count-1);
            var point = spawnPoints[randomIndex];
            //is randomIndex between any part of the snake.
            if (snakeControllerObject.Snake.Count > 0)
            {
                foreach (var segements in snakeControllerObject.Snake)
                {
                    var mesh = segements.GetComponent<MeshFilter>().mesh;
                    if (mesh.bounds.Contains(point))
                    {
                        //recursion!
                        this.getSafeSpawnPoint();
                    }
                }
            }
            return point;
        }
     
        void OnTriggerEnter(Collider other) {
            if(other.gameObject.tag == "Player") {
                StartCoroutine(fxCoroutine);
            }
        }

        private void OnDisable() {
            //just incase you're in action when we move on.
            StopCoroutine(fxCoroutine);
        }

        private IEnumerator Respawn() {
            if (eatParticleFx) {
                appleMeshRender.enabled = false;
                eatParticleFx.Play();
                while (eatParticleFx.isPlaying) {
                    yield return null;
                }
                Spawn();
            }
            yield break;
        }
        /*
* create two new function, the first is a Build funstion, we will build a list of available spawn points from the surface traiangles. The second is the Spawn() function. 
* This function will be responsible for spawning the apple in an appropriate space.
* In the build function we will use Unity' built in vertices and triangles array to find the center point(Centroid) of every triangle on the planets surface.
* These centroid will be the only places a apple can spawn. We will store these points in a List.
* For Future modification we can add obsticles by placing then anywhere there isn't a appleSpawnPoint. 
*/
    }
}
