﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SnakePlanet {
    public class SnakeController : MonoBehaviour {
        public float _moveSpeed = 1500f;
        public float _turnSpeed = 500f;
        public float _turnRotationSpeed = 25f;
        public float _turnRotationSeekSpeed = 0.25f;
        public float _reverseSpeed = 0.0f;
        [SerializeField]
        private float _deadZone = 0.1f;
        public float _maxVelocity = 30f;
        [SerializeField]
        private float _moveSpeedMultiplier = 5f;
        private float _maxSpeed = 5f;
        [SerializeField]
        private List<GameObject> _bodySegments;
        [SerializeField]
        private GameObject _bodySegmentPrefab;
        private bool _grounded = false;

        private float _moveForce;
        private float _torqueForce;
        private Transform _rigidBodyTransform;
        private Rigidbody _rigidBody;
        
        
        //reference variables used in Mathf smoothDampAngle
        private float _rotationVelocity;

        public List<GameObject> Snake {
            get {
                return _bodySegments;
            }

            set {
                _bodySegments = value;
            }
        }



        // Use this for initialization
        void Awake() {
            //self rigidbody and up awareness
            _rigidBodyTransform = transform;
            _rigidBody = GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        void Update() {
            //using the deadzone figure out if we're moving forwards or back.
            _moveForce = -Input.GetAxisRaw("Vertical");
            if (_moveForce > _deadZone) {
                _moveForce = 0.0f;
            }
               
            _torqueForce = Input.GetAxisRaw("Horizontal");
            if (Mathf.Abs(_torqueForce) < _deadZone)
                _torqueForce = 0.0f;
        }
        //For are physic calculations
        void FixedUpdate() {
            //** OLD BUSTED **//
            //must transform our moveDirection to Worldspace
            //_rigidBody.MovePosition(_rigidBody.position + transform.TransformDirection(_moveDirection) * _moveSpeed * Time.deltaTime);

            //**NEW HOTNESS**//
            if (Grounded) {
                var forwardForce = _rigidBodyTransform.forward * _moveSpeed * _moveForce;
                forwardForce *= Time.deltaTime * _rigidBody.mass;
                _rigidBody.AddForce(forwardForce);
            }
            
            var turnTorque = _rigidBodyTransform.up * _turnSpeed * _torqueForce;
            turnTorque *= Time.deltaTime * _rigidBody.mass;

            _rigidBody.AddTorque(turnTorque);

            //visual hotness, bank turning on Z axis
            Vector3 bankRotation = _rigidBodyTransform.eulerAngles;
            bankRotation.z = Mathf.SmoothDampAngle(bankRotation.z, _moveForce * -_turnRotationSpeed, ref _rotationVelocity, _turnRotationSeekSpeed);
            _rigidBodyTransform.eulerAngles = bankRotation;
            
        }

        public bool Grounded { get { return _grounded; } set { _grounded = value; } }
        
        public void GenerateSegment() {
            if(_bodySegments.Count == 0) {
                _bodySegments = new List<GameObject>();
            }
            GameObject _head = new GameObject();
            var prefab = Instantiate<GameObject>(_bodySegmentPrefab, Vector3.forward, _head.transform.rotation);
            _bodySegments.Add(prefab);
        }

    }
}
