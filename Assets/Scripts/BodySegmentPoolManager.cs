﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace SnakePlanet {
    public class BodySegmentPoolManager : MonoBehaviour {
#region FIELDS
        public GameObject bodySegment;
        public List<GameObject> bodySegmentPoolList;
        public int poolSize = -1;

        private GameObject selectedBodyPart;
        private Transform planetCore;
      
        public GameObject SelectedBodyPart {
            get {
                return selectedBodyPart;
            }

            set {
                selectedBodyPart = value;
            }
        }
#endregion
        // Use this for initialization
        void Start() {
            planetCore = GameObject.FindWithTag("Planet").GetComponent<Transform>();
            //NOTE cast to ref varianle as gameobject.transform isn't a property but a unity `magic` method
            //best to cast when re-using the transform object.
            var bodyTransform = bodySegment.transform;
            if(poolSize < 1) {
                poolSize = Mathf.CeilToInt((planetCore.lossyScale.x * bodyTransform.position.x) + 
                                            (planetCore.lossyScale.z * bodyTransform.position.z));
            }
            Build();
        }

        private void Build() {
            bodySegmentPoolList = new List<GameObject>();
            bodySegmentPoolList.Add(bodySegment);
            for (int i = 1; i < poolSize; i++) {
                //copy a bodySegment into the list.
                bodySegmentPoolList.Add(
                    Instantiate<GameObject>(bodySegment, planetCore));
            }
        }
        
        //not really used how the logic of snake plays out. but here it is.
        void ResetBodyPart() {
            bodySegmentPoolList.Add(SelectedBodyPart);
        }
        void PositionBodyPart() {
            RemoveBodyPart();
            //attach to lastFixed Joint off the snake which is identified with a tag.
            var tail = GameObject.FindWithTag("Tail").GetComponent<Transform>();
            selectedBodyPart.tag = "Tail";

            if (tail.tag == "Tail") {
                //change tags as there's a new tail in town.
                tail.tag = "Body";
            }
            else { 
                //only the head on the board.
                tail = GameObject.FindWithTag("Player").GetComponent<Transform>();
            }
            var spingJoint = selectedBodyPart.GetComponent<SpringJoint>();
            spingJoint.connectedBody = tail.GetComponent<Rigidbody>();
        }
        void RemoveBodyPart() {
            SelectedBodyPart = bodySegmentPoolList[bodySegmentPoolList.Count - 1];
        }
    }
}
